#this file contains the playlist selection box

import wx
import os
import wx.lib.mixins.listctrl as listmix
from Sortedlist import SortedListFrame
from Util import ReadFile
import playListsLearner

#playlist of songs
class ListCtrl(wx.ListCtrl, listmix.ListCtrlAutoWidthMixin):
    def __init__(self, parent, ID, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=0):
        wx.ListCtrl.__init__(self, parent, ID, pos, size, style)
        listmix.ListCtrlAutoWidthMixin.__init__(self)

class PlaylistPanel(wx.Panel,listmix.ColumnSorterMixin):
    def __init__(self,parent):
        #row of the file list
        self.__Row__ = 0
        wx.Panel.__init__(self,parent)
        ListText = self.__GetListText__() 
        self.__List__ = self.__GetList__()
        PanelBox = self.__GetVeriticalBox__()
        FButton = self.__GetBrowseButton__()
        CButton = self.__GetCompileButton__()
        TButton = self.__GetTrainButton__()
        PanelBox.Add(ListText, 0, wx.LEFT|wx.TOP|wx.RIGHT|wx.FIXED_MINSIZE, 20)
        PanelBox.Add(self.__List__,1, wx.ALL|wx.EXPAND, 10)
        ButtonBox = self.__GetHorizntalBox__()
        ButtonBox.Add(TButton,0,wx.RIGHT|wx.FIXED_MINSIZE,10)
        ButtonBox.Add(FButton,0,wx.RIGHT|wx.FIXED_MINSIZE,10)
        ButtonBox.Add(CButton,0,wx.FIXED_MINSIZE)
        PanelBox.Add(ButtonBox,0,wx.ALL|wx.FIXED_MINSIZE, 20)
        

        #listmix.ColumnSorterMixin.__init__(self, 2)
        #show the panel
        self.SetSizer(PanelBox)
        self.SetAutoLayout(True)
        
    #playlist text
    def __GetListText__(self):
        Text = wx.StaticText(self, -1, "Playlist")
        Text.SetFont(wx.Font(12, wx.SWISS, wx.NORMAL, wx.LIGHT))
        Text.SetSize(Text.GetBestSize())
        return Text
        
    #actual playlist
    def __GetList__(self):
        List = ListCtrl(self,-1,wx.DefaultPosition,wx.DefaultSize,style = wx.LC_REPORT|wx.BORDER_SUNKEN|wx.LC_SORT_ASCENDING)
        #list columns
        List.InsertColumn(0, "Title")
        List.InsertColumn(1, "Artist")
        return List

    '''
    # Used by the ColumnSorterMixin, see wx/lib/mixins/listctrl.py
    def GetListCtrl(self):
        return self.__List__
    '''

    def __GetVeriticalBox__(self):
        return wx.BoxSizer(wx.VERTICAL)

    def __GetHorizntalBox__(self):
        return wx.BoxSizer(wx.HORIZONTAL)
        
    #add button
    def __GetBrowseButton__(self):
        Button = wx.Button(self, -1, "Add",(20,20))
        self.Bind(wx.EVT_BUTTON, self.__OnFile__, Button)
        return Button
    
    #compile button
    def __GetCompileButton__(self):
        Button = wx.Button(self, -1, "Compile",(20,20))
        self.Bind(wx.EVT_BUTTON, self.__OnCompile__, Button)
        return Button
    
    #train button
    def __GetTrainButton__(self):
        Button = wx.Button(self, -1, "Train",(20,20))
        self.Bind(wx.EVT_BUTTON, self.__OnTrain__, Button)
        return Button
    
    #train callback
    def __OnTrain__(self,event):

        self.__network__ = playListsLearner.train()
        '''
        #to do, add the traning algorithm
        Count = 0
        Max = 10
        dlg = wx.ProgressDialog("Train","Traning....",maximum=Max,parent=self,style = wx.PD_CAN_ABORT|wx.PD_ELAPSED_TIME)
        Continue = True
        while Continue and Count < Max:
            Count += 1
            wx.MilliSleep(100)
            (Continue,Skip) = dlg.Update(Count)
        
        if not Continue:
            dlg.Destroy()
        '''
        
    #compile callback
    def __OnCompile__(self,event):
        #get the sorted data
        Songs = playListsLearner.songClassifier(self.__network__,self.__testDataSet__,self.__Songs__)
        #Songs = ReadFile(os.getcwd() + "/sortedset.json")
        Win = SortedListFrame(Songs,self, -1, "SortedList", size=(650, 500),style = wx.DEFAULT_FRAME_STYLE)
        Win.Center()
        Win.Show(True)

    #add callback
    def __OnFile__(self,event):
        JsonFile = "Json(*.json)|*.json|" \
           "All files (*.*)|*.*"

        #open a file dialog
        dlg = wx.FileDialog(
            self, message="Choose a file",
            defaultDir=os.getcwd(), 
            defaultFile="",
            wildcard=JsonFile,
            style=wx.OPEN | wx.CHANGE_DIR
        )
        
        if dlg.ShowModal() == wx.ID_OK:
            Path = dlg.GetFilename()
            self.__Songs__ = ReadFile(Path)
            self.__testDataSet__ = playListsLearner.makeTestDataSet(self.__Songs__)
            #self.__AddFile__(Files)
            dlg.Destroy()
            #display the data
            for Song in self.__Songs__:
                self.__List__.InsertStringItem(self.__Row__, Song["title"])  
                self.__List__.SetStringItem(self.__Row__, 1, Song["artist"])
                self.__Row__ += 1
            
    #add a row
    def __AddFile__(self, Files):
        #read in the trainset
        #Path = os.getcwd() + "tran"
        #ReadFile()
        for File in Files:
            self.__List__.InsertStringItem(self.__Row__, File)  
            self.__Row__ += 1
