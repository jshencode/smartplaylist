import wx
from wx.lib.wordwrap import wordwrap
import os
from Playlist import PlaylistPanel

# This is how you pre-establish a file filter so that the dialog
# only shows the extension(s) you want it to.
wildcard = "Python source (*.py)|*.py|" \
           "All files (*.*)|*.*"

class Frame(wx.Frame):
    def __init__(self, title):
        wx.Frame.__init__(self, None, title=title,pos=wx.DefaultPosition,size=(650,500))
        self.Bind(wx.EVT_CLOSE, self.__OnClose__)
        MenuBar = wx.MenuBar()
        #file menu
        menu = wx.Menu()
        menu.Append(101, "O&pen File\tCtrl-F", "Open a File.")
        self.Bind(wx.EVT_MENU, self.__OnFile__,id=101)
        menu.Append(102, "E&xit\tAlt-F4", "Close the program.")
        self.Bind(wx.EVT_MENU, self.__OnClose__,id=102)
        MenuBar.Append(menu, "&File")
        #help menu
        menu = wx.Menu()
        menu.Append(201, "A&bout", "Info about this program")
        self.Bind(wx.EVT_MENU, self.__OnAbout__,id=201)
        MenuBar.Append(menu, "&Help")        
        self.SetMenuBar(MenuBar)
        #self.statusbar = self.CreateStatusBar()
        
        #playlist panel
        PlaylistPanel(self)

    def __OnFile__(self,event):
        #open a file dialog
        dlg = wx.FileDialog(
            self, message="Choose a file",
            defaultDir=os.getcwd(), 
            defaultFile="",
            wildcard=wildcard,
            style=wx.OPEN | wx.MULTIPLE | wx.CHANGE_DIR
        )
        
        if dlg.ShowModal() == wx.ID_OK:
            names = dlg.GetFilenames()            
            #get the paths
            #paths = dlg.GetPaths()
            #for path in paths:
            
        dlg.Destroy()
        
    def __OnAbout__(self, event):
        #info box
        info = wx.AboutDialogInfo()
        info.Name = "SmartPlaylist"
        info.Version = "1.0"
        info.Copyright = "(C) 2013 Spring CSCE670"
        info.Description = wordwrap(
            "",
            350, wx.ClientDC(self))
        info.WebSite = ("http://courses.cse.tamu.edu/caverlee/csce670/")
        info.Developers = [ "Jian Shen",
                            "Yixin Geng",
                            "Cong Hui" ]
        wx.AboutBox(info)

        
    def __OnClose__(self, event):
        dlg = wx.MessageDialog(self,
                               "Confirm Exit?",
                               "Exit", wx.OK|wx.CANCEL|wx.ICON_QUESTION)
        result = dlg.ShowModal()
        dlg.Destroy()
        if result == wx.ID_OK:
            self.Destroy()

def main():
    app = wx.App(redirect=True) 
    top = Frame("SmartPlaylist")
    top.Center()
    top.Show()
    app.MainLoop()

    
if __name__ == "__main__":
    main()

