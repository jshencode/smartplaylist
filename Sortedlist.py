import wx
import wx.lib.mixins.listctrl as listmix

#playlist of songs
class ListCtrl(wx.ListCtrl, listmix.ListCtrlAutoWidthMixin):
    def __init__(self, parent, ID, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=0):
        wx.ListCtrl.__init__(self, parent, ID, pos, size, style)
        listmix.ListCtrlAutoWidthMixin.__init__(self)


class SortedListFrame(wx.Frame):
    def __init__(self,Songs,Parent,ID,title,pos=wx.DefaultPosition,size=wx.DefaultSize,style=wx.DEFAULT_FRAME_STYLE):
        wx.Frame.__init__(self,Parent,ID,title,pos,size,style)
        self.__Row__ = 0
        self.__Songs__ = Songs
        self.__List__ = self.__GetList__()
        List1 = sorted(self.__Songs__,key = lambda Dict: Dict["lists"][0],reverse = True)
        List2 = sorted(self.__Songs__,key = lambda Dict: Dict["lists"][1],reverse = True)
        List3 = sorted(self.__Songs__,key = lambda Dict: Dict["lists"][2],reverse = True)
        
        #display the data
        for i in xrange(50):
            Row = self.__List__.InsertStringItem(self.__Row__, List1[i]["title"] + " : " + List1[i]["artist"])  
            self.__List__.SetStringItem(Row,1, List2[i]["title"] + " : " + List2[i]["artist"])  
            self.__List__.SetStringItem(Row,2, List3[i]["title"] + " : " + List3[i]["artist"])  
            self.__Row__+= 1
        PanelBox = self.__GetVeriticalBox__()
        PanelBox.Add(self.__List__,1, wx.ALL|wx.EXPAND, 10)
        #show the panel
        self.SetSizer(PanelBox)
        self.SetAutoLayout(True)
       
     #actual playlist
    def __GetList__(self):
        List = ListCtrl(self,-1,wx.DefaultPosition,wx.DefaultSize,style = wx.LC_REPORT|wx.BORDER_SUNKEN|wx.LC_SORT_ASCENDING)
        #list columns
        List.InsertColumn(2, "List1")
        List.InsertColumn(3, "List2")
        List.InsertColumn(4, "List3")
        return List
    
    def __GetVeriticalBox__(self):
        return wx.BoxSizer(wx.VERTICAL)


        
