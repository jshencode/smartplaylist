import json

#read the json file
def ReadFile(Path):
    F = open(Path,"r")
    Data = json.load(F)
    F.close()
    return Data
